# Data Visualization Capstone Project

## Algoritma

Algoritma is a data science education center based in Jakarta. We organize workshops and training programs to help working professionals and students gain mastery in various data science sub-fields: data visualization, machine learning, data modeling, statistical inference etc. Visit our website for all upcoming workshops.

## Data Visualisation Capstone Project

After having learned and explored appropriate techniques on visualizing data, students are required to deploy an interactive dashboard web application using a shiny server which contain any plotting objects such as plotly and/or plotly express that display useful insights.

### First Objective

Before you making the dashboard, let’s answer this question first to help you creating a dashboard with useful insight.

### What

What is the dashboard about?

This question is self explanatory, you should know what is it about, what problem you try to solve with this dashboard, what story you try to tell to your audience.

### Who

Who is the user of your dashboard?

Knowing the user of your dashboard is very important. What division or what kind of people using this dashboard. Do you need a detail or more practical dashboard? When your user is on operational level you need a detailed dashboard but when your user is on managerial level you need simple and general dashboard that can convey the insight quickly.

### Why

Why you choose that data?

How much your understanding of that data, Is that data can solve your question? Why do you choose that variable, are they really corelated? Important to know why your choose that data so you don’t create a misleading insight which very dangerous.

### When

When is the data collected?

Is it still relevant? For example You can’t use the data from 80s to describe how’s the traffic at current date. Since the trend is everchanging so does the answer to your question, can those very old data answer your question? Irrelevant data can create a misleading insight.

### Where

Where you put your plot, valuebox, or input etc?

Make a simple layout design, so you have a image how your end product will look like. Is it tidy enough? Easy enough for your user to understand it? Always follow 5 seconds rule. Your dashboard should provide the relevant information in about 5 seconds.

### How

How your dashboard answer your question, hypothesis, or problem you try to solve?

Are you using a right plot? A right variable? Always start from your problem, make sure you use a right plot for right problem. For example what plot you use for see your data distribution? Are you using density plot or line plot?

## Rubrics

In addition, students are given the freedom to use their own dataset or past datasets from previous classes. Below are the rubrics for assessment and grading, Students will get the point(s) if they :

#### Layout or User Interface Appearence

- (1 point) Create Dashboard Name, App instance

    + ![dashboard_image](img/dashboard_name.png)

- (2 points) Have tidy page layout
    
    + We don’t expect you to be a great UI designer. However, your dashboard page should be tidy and clean enough to watch. Some considerations to help you create a tidy page including:
        1.  Should not too long and has distinct topics
        2.  Be consistent, e.g. all texts are in English and consistent color themes
        3.  Page should be filled thoroughly with content without leaving any blank spaces in a full-width length of a page.

    + ![tidy_layout](img/tidy_layout.png)

- (2 points) Have tidy plot layout

    + Have clear plot title and axis title
    + Text is readable and contrast with the background (if background is in dark color, the text should be in light color, and vice versa)
    + Ranking is presented with clear order (ascending or descending), for example when you present top 10 product name or top 10 customer
    + Have consistent themes for all plots
    + No overlapping axis text, long text should not be rotated with 90% degree rotation.

- (2 points) Choosing right color scheme and theme

    + Color must be carefully chosen to serve a purpose and it must be clear and do not distract the reader. One of the most common pitfalls is using color for bar charts when a large number of categories are present. You can read more about issues of color usage in [this book’s chapter](https://clauswilke.com/dataviz/color-pitfalls.html).
    + ![color_scheme](img/color_scheme.png)

### Plot (Visualization)

- (1 points) Using interactive plot

    + All plots should be presented as interactive plots. You can either use plotly, plotly express.

- (2 points) Using min. 2 plot type

    + The dashboard should contain at least 2 different plot types. We expect you can explore different visualizations to convey different information. For example, a dashboard contains 2 different plots: a bar chart and a line plot. The number of plots itself is not limited.

- (2 points) Choosing the appropriate plot type

    + All information should be presented with appropriate plots. For example, if you want to show categorical ranking, you can use bar chart or lollipop chart. You can refer to [data-to-viz](https://www.data-to-viz.com/) for guidance.

- (2 points) Creating plots that tell a clear story

    + All plots should have clear information and are easy to understand. There should be at least a plot title and clear axis title. You can refer to [this notes](https://clauswilke.com/dataviz/introduction.html#ugly-bad-and-wrong-figures) regarding this problem.
    + ![clear_story](img/clear_story.png)

- (1 points) Have appropriate plot hover text

    + The plot should have a customized hover text / tooltip that has a clear and easy to read popup text.

### Input

- (2 points) Using min. 1 different input type

    + The dashboard should contain at least one [input type](https://dash.plotly.com/dash-core-components). For example: checklist, dropdown, input, and slider. 

- (2 points) Choosing appropriate input type

    + The input should have an appropriate user interface. 

- (2 points) Demonstrating useful input(s)

    + The dashboard should have input widgets that would give the user the ability to explore the data. Some useful input including filtering data with slider input or selecting different categories with select input. Less useful input including changing the color of the plot.

- (4 points) Demonstrating reactivity from the input (callback)

    + The plot should be able to react to the change given by input using callback

### Deploy

- (5 points) Successfully deploying to [railway.com](https://railway.app/) or another VPS (Virtual Private Server)

## Total Score

If you achieved all those criteria you will get total 30 points.

## Project Timeline

1. Briefing Capstone Data Visualization

    - Wednesday, 23 November 2022

2. Mentoring Capstone Data Visualization

    - Thursday, 24 November 2022
    - Tuesday, 29 November 2022
    - Wednesday, 30 November 2022
    - Tuesday, 6 December 2022
    - Tuesday, 13 December 2022

3. Submission

    - Friday, 16 December 2022


## Reference

- [Fundamental Data Visualization](https://clauswilke.com/dataviz/)

### Dash Plotly

- [Dash Enterprise](https://dash.gallery/Portal/?_gl=1*bdupbj*_ga*NjQ2ODkyNDIyLjE2MjE1ODgyNTk.*_ga_6G7EE0JNSC*MTY2OTE2OTA5NC41Ni4xLjE2NjkxNjkxNzQuMC4wLjA.)
- [Dash Plotly](https://dash.plotly.com/introduction)
- [Dash Core Component](https://dash.plotly.com/dash-core-components)
- [Dash HTML Component](https://dash.plotly.com/dash-html-components)
- [Dash Bootstrap Component](https://dash-bootstrap-components.opensource.faculty.ai/)
- [Styling CSS](https://www.w3schools.com/css/default.asp)
- [Bootstrap 5 Cheatsheet](https://bootstrapcreative.com/resources/bootstrap-5-cheat-sheet-classes-index/)
- [Bootstrap 5 Documentation](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
- [Add CSS and JS](https://dash.plotly.com/external-resources)
- [Multipage Dash](https://dash.plotly.com/urls)

### Plotly

- 📊 [Data to Viz](https://www.data-to-viz.com/)
- 🛠 [Plotly Express](https://plotly.com/python/plotly-express/)
- 📈 Basic Plot
    + [Bar](https://plotly.com/python/bar-charts/)
    + [Histogram](https://plotly.com/python/histograms/)
    + [Box](https://plotly.com/python/box-plots/)
    + [Line](https://plotly.com/python/line-charts/)
    + [Scatter](https://plotly.com/python/line-and-scatter/)
    + [Heatmap](https://plotly.com/python/2D-Histogram/)
    + [Choropleth](https://plotly.com/python/choropleth-maps/)
- 🎨 Color
    + [Continuous color](https://plotly.com/python/builtin-colorscales/)
    + [Discrete color](https://plotly.com/python/discrete-color/)
    + [Color name](https://www.w3schools.com/colors/colors_names.asp)
- 🔎 Advance Plot
    + [Layouting](https://plotly.com/python/reference/layout/)
        + [Example](https://plotly.com/python/styling-plotly-express/#styling-figures-made-with-plotly-express)
        + Documentation:
            + [update_layout](https://plotly.com/python/reference/layout/)
            + [axes and subplots](https://plotly.com/python/reference/layout/xaxis/)
    + [Animation](https://plotly.com/python/animations/)
    + [Adding traces](https://plotly.com/python/creating-and-updating-figures/#adding-traces)